package com.twuc.webApp.web;

import com.twuc.webApp.domain.Staff;
import com.twuc.webApp.domain.StaffRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/api")
public class Controller {

    @Autowired
    private StaffRepository staffRepository;

    @Autowired
    private EntityManager entityManager;

    private Set<Long> ids = new HashSet<>();

    public Controller() {
        ids.add(0L);
    }

    @PostMapping("/staffs")
    public ResponseEntity createStaff(@RequestBody Staff staff){
        if(staff.getFirstName()==null||staff.getLastName()==null
                ||staff.getFirstName().length() > 64
                || staff.getLastName().length() > 64){
            throw new IllegalArgumentException("firstName lastName not null");
        }
        Long curId = Collections.max(ids);
        curId += 1;
        staff.setId(curId);
        ids.add(curId);

        staffRepository.save(staff);



        return ResponseEntity.status(201).body("{\"Location\":\"/api/staffs/"+(curId)+"\"}");
    }


    @GetMapping("/staffs/{staffId}")
    public ResponseEntity<Staff> getStaffById(@PathVariable Long staffId){
        Staff staff = staffRepository.findAllById(staffId);
        if(staff==null){
            return ResponseEntity.status(404).build();
        }
        return ResponseEntity
                .status(200)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .body(staff);
    }

    @GetMapping("/staffs")
    public ResponseEntity<List<Staff>> getAllStaff(){
        List<Staff> staffList = staffRepository.findAll();
        System.out.println(staffList.size());
        return ResponseEntity.status(200).body(staffList);
    }








    @ExceptionHandler({IllegalArgumentException.class})
    private ResponseEntity handleIllegal(Exception e){
        return ResponseEntity.status(400).build();
    }

}
