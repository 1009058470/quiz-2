package com.twuc.webApp.domain;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface StaffRepository extends JpaRepository<Staff,Long> {

    Staff findAllById(Long id);
    List<Staff> findAll();
}
