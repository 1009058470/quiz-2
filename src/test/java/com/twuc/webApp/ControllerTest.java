package com.twuc.webApp;

import com.twuc.webApp.domain.Staff;
import com.twuc.webApp.domain.StaffRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class ControllerTest extends ApiTestBase {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    StaffRepository staffRepository;

    @Test
    void should_return_201_when_create_staff_success() throws Exception {
        mockMvc.perform(post("/api/staffs"))
                .andExpect(status().is(201));
    }

    @Test
    void should_create_with_name() throws Exception {
        mockMvc.perform(post("/api/staffs").contentType("application/json").content("{\"firstName\": \"Rob\",\"lastName\": \"Hall\"}"))
                .andExpect(content()
                        .string("{\"Location\":\"/api/staffs/1\"}"));
    }

    @Test
    void should_return_400_when_firstName_or_lastName_is_not_true() throws Exception {
        mockMvc.perform(post("/api/staffs").contentType("application/json").content("{\"firstName\": \"rosssssssssssssssssrosssssssssssssssssrosssssssssssssssssrosss\",\"lastName\": \"Hall\"}"))
                .andExpect(status().is(400));
    }

    @Test
    void should_return_content_when_staffid_have() throws Exception {
        staffRepository.save(new Staff("1","1"));
        mockMvc.perform(get("/api/staffs/1")).andExpect(content().string("{\"id\":1,\"firstName\":\"1\",\"lastName\":\"1\"}"));
    }

    @Test
    void should_return_404_when_not_have() throws Exception {
        mockMvc.perform(get("/api/staffs/1"))
                .andExpect(status().is(404));
    }

    @Test
    void should_return_null_when_staff_in_db_si_null() throws Exception {
        mockMvc.perform(get("/api/staffs"))
                .andExpect(status().is(200))
                .andExpect(content().string("[]"));
    }

    @Test
    void should_return_all_staffs_in_db() throws Exception {
        staffRepository.save(new Staff("1","1"));
        staffRepository.save(new Staff("2","2"));
        mockMvc.perform(get("/api/staffs"))
                .andExpect(status().is(200))
                .andExpect(content().string("[{\"id\":1,\"firstName\":\"1\",\"lastName\":\"1\"},{\"id\":2,\"firstName\":\"2\",\"lastName\":\"2\"}]"));
    }
}
