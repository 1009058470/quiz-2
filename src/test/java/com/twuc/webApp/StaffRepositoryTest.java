package com.twuc.webApp;

import com.twuc.webApp.domain.Staff;
import com.twuc.webApp.domain.StaffRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class StaffRepositoryTest extends JpaTestBase{
    @Autowired
    private StaffRepository staffRepository;

    @Test
    void should_save_not_null_in_db() {
        Staff staff = new Staff("first","last");
        flushAndClear(entityManager -> {
            entityManager.persist(staff);
        });

        flushAndClear(entityManager -> {
            Staff staff1 = entityManager.find(Staff.class, 1L);
            System.out.println(staff1.getFirstName());
            System.out.println(staff1.getLastName());
            assertEquals(new Staff("first","last"),staff1);
        });
    }


    @Test
    void should_all_staff_in_db() {
        flushAndClear(entityManager -> {
            staffRepository.save(new Staff("1","1"));
            staffRepository.save(new Staff("2","2"));
        });

        //then
        flushAndClear(entityManager -> {
            Staff Staff = staffRepository.findAllById(1L);
            assertEquals(new Staff("1","1"),Staff);
        });

    }
}
