package com.twuc.webApp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;

import javax.persistence.EntityManager;
import java.util.function.Consumer;

@ActiveProfiles("test")
@DataJpaTest(showSql = false)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public abstract class JpaTestBase {
    @Autowired
    private EntityManager entityManager;

    private EntityManager getEntityManager() {
        return entityManager;
    }

    protected void flush(Consumer<EntityManager> consumer) {
        final EntityManager em = getEntityManager();
        consumer.accept(em);
        em.flush();
    }

    protected void flushAndClear(Consumer<EntityManager> consumer) {
        final EntityManager em = getEntityManager();
        consumer.accept(em);
        em.flush();
        em.clear();
    }

    protected void run(Consumer<EntityManager> consumer) {
        final EntityManager em = getEntityManager();
        consumer.accept(em);
    }
}

